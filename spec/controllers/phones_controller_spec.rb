require 'rails_helper'

RSpec.describe PhonesController, type: :controller do

  describe "GET #get" do
    it "returns http success" do
      get :read, id: 1
      expect(response).to have_http_status(:success)
    end
  end

end
