require 'rails_helper'

describe GsmarenaProvider do
  #Bad practive but I don't have time to mocking up the site responce
  context "ok case" do
    it "get_data" do      
      expect(GsmarenaProvider.get_data('xiaomi redmi note 3').count).to eq(7)
    end
    it "get_detailed_data" do
      phone = GsmarenaProvider.get_data('xiaomi redmi note 3')[0]
      phone = GsmarenaProvider.get_detailed_data(phone)
      expect(phone.display_size).not_to be nil
      expect(phone.network).not_to be nil
    end
  end
  
end