class PhonesController < ApplicationController

  before_action :get_provider, only: :read
  
  def read
    render json: DataProvider.new(@provider.get_data(params[:id]))
  end
  
  def index
  end

  private

  def get_provider
    render json: { error: 'Please pass keyprase' } and return if params[:id].blank?
    @provider = GsmarenaProvider
  end
end
