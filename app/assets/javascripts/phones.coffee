$(document).ready ->
  $('#searchtext').keypress (e)->
    if (e.which==13)
      $('#searchbutton').click()
  $('#searchbutton').click ->
    keyphrase = $('#searchtext').val().trim()
    callback = (results)->
      $('#results').val(results)
    $.ajax
      type: 'get'
      url: '/phones/' + keyphrase
      data: keyphrase
      accepts: json: 'application/json'
      dataType: 'json'
      success: (data) ->
        $('#results').html('')
        print_phone(data['provider'])
      error: (data) ->
        if data['status'] == 404
          alert('please provide the search string')
          return
        alert "Oops, something gets wrong"
  
  print_phone = (phone)->
    if phone == 'Nothing found'
      alert('no results')
      return
    result = '<li class="list-group-item">'
    for key, val of phone
      result += '<ul><img src="'+ val['img'] + '"></img></li>'
      result += '<li><a href="' + val['page'] + '">' + val['title'] + '</a></li>'
      result += '<li>' + val['spec'] + '</li>'
      result += '</li></ul> <li class="list-group-item">'
    $('#results').append(result)
