class GsmarenaProvider
  URL = 'http://www.gsmarena.com/'

  def self.get_data(phones)
    agent = Mechanize.new
    page = agent.get URL
    search_form = page.form_with :id => "topsearch"
    search_form.field_with(:id => "topsearch-text").value = phones

    search_results = agent.submit search_form
    parse_search_results(search_results)
  end

  def self.get_detailed_data(phone)
    agent = Mechanize.new
    page = agent.get URL + phone.page
    phone.network = page.css('td:contains("GSM")').text
    phone.display_size = page.css('td:contains("inches")').text
    phone
  end

private

  def self.parse_search_results(search_results)
    return 'Nothing found' if search_results.css('.makers li').count == 0
    parsed_result = []
    search_results.css('.makers li').each do |phone|
      parsed_result << Phone.new(phone, URL)
    end
    parsed_result
  end
end

