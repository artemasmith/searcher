class Phone
  @img
  @title
  @spec
  @page
  @network
  @display_size

  attr_accessor :img, :title, :spec, :page, :network, :display_size

  def initialize(phone, base_url = nil)
    @img = phone.css('img').first.attributes['src'].value
    @title = phone.css('img').first.attributes['title'].value.split('.')[0]
    @spec = phone.css('img').first.attributes['title'].value
    @page = phone.css('a').first.attributes['href'].value
    @page = base_url + @page if base_url.present?
  end
end