class DataProvider
  @provider
  
  def initialize(provider)
    @provider = provider
  end

  def get_data(keyword)
    @provider.get_data(keyword)
  end
end