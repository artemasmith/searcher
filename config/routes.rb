Rails.application.routes.draw do
  get 'phones/:id', to: 'phones#read'
  # resources :phones, only: [:index, :read]
  root to: 'phones#index'

end
